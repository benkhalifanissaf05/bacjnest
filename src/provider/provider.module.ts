import { Module } from '@nestjs/common';
import { ProviderService } from './provider.service';
import { ProviderController } from './provider.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProviderSchema } from './entities/provider.entity';

@Module({
  imports:[MongooseModule.forFeature([{name: 'providers', schema: ProviderSchema}])],
  controllers: [ProviderController],
  providers: [ProviderService]
})
export class ProviderModule {}
