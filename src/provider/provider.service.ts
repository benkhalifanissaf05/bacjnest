import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDto } from './dto/update-provider.dto';
import { ProviderDocument } from './entities/provider.entity';
import { Provider} from './provider.interface';
@Injectable()
export class ProviderService {
  constructor(@InjectModel ('providers') private ProviderModel: Model<ProviderDocument>){}
  async  create(createProviderDto: CreateProviderDto): Promise<Provider> {
     const creatProvider= new this.ProviderModel(createProviderDto);
     return creatProvider.save();
   }
 
  async  findAll() : Promise<Provider[]>{
    
    
     return this.ProviderModel.find().exec();
   }
 
   async findOne(id: string) {
     return this.ProviderModel.findById(id).exec();
   }
 
   update(id: string, updateProviderDto: UpdateProviderDto) {
     return this.ProviderModel.findByIdAndUpdate({_id:id}, updateProviderDto, {new : true});
   }
 
   remove(id: string) {
     return this.ProviderModel.findByIdAndDelete({_id:id}, {new: true});
   }
}
