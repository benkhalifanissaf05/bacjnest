
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, Types } from "mongoose";
import { type } from "os";

export type OrderDocument =HydratedDocument<Order>;
@Schema()
export class Order {

    @Prop()
    ref: string;
    @Prop()
    description: string;
    @Prop()
    qte_total: string;
    @Prop()
    price_total: string;
    @Prop()
    state:string;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }])
    products: Types.ObjectId;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'delliveries' })
    dellivery: Types.ObjectId;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'customers' })
    customer: Types.ObjectId;
}
export const OrderSchema= SchemaFactory.createForClass(Order);