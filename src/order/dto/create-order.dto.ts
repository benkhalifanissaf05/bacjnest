export class CreateOrderDto {
    ref: string;
    description: string;
    qte_total: string;
    price_total: string;
    state:string;
}
