import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import {  OrderDocument } from './entities/order.entity';
import { Order} from './order.interface'
@Injectable()
export class OrderService {
  constructor(@InjectModel ('orders') private OrderModel: Model<OrderDocument>){}
  async  create(createOrderDto: CreateOrderDto): Promise<Order> {
     const creatOrder= new this.OrderModel(createOrderDto);
     return creatOrder.save();
   }
 
  async  findAll() : Promise<Order[]>{
    console.log("ordersss ");
    
     return this.OrderModel.find().exec();
   }
 
   async findOne(id: string) {
     return this.OrderModel.findById(id).exec();
   }
 
   update(id: string, updateOrderDto: UpdateOrderDto) {
     return this.OrderModel.findByIdAndUpdate({_id:id}, updateOrderDto, {new : true});
   }
 
   remove(id: string) {
     return this.OrderModel.findByIdAndDelete({_id:id}, {new: true});
   }
}
