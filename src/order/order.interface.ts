import { Types } from "mongoose";

export interface Order { 
    ref: string;
    description: string;
    qte_total: string;
    price_total: string;
    state: string;
    products: Types.ObjectId;
    dellivery: Types.ObjectId;
    customer: Types.ObjectId;
}
