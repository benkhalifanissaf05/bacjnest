import { Types } from "mongoose";

export interface Customer {
    localization: string;
    orders: Types.ObjectId;

}
