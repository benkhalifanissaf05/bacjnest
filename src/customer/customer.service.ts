import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Customer } from './customer.interface';
import {CustomerDocument} from './entities/customer.entity';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';

@Injectable()
export class CustomerService {
  constructor(@InjectModel ('customers') private CustomerModel: Model<CustomerDocument>){}
  async  create(createCustomerDto: CreateCustomerDto): Promise<Customer> {
     const creatCustomer= new this.CustomerModel(createCustomerDto);
     return creatCustomer.save();
   }
 
  async  findAll() : Promise<Customer[]>{
     return this.CustomerModel.find().select("-__v").exec();
   }
 
   async findOne(id: string) {
     return this.CustomerModel.findById(id).select("-__v").exec();
   }
 
   update(id: string, updateCustomerDto: UpdateCustomerDto) {
     return this.CustomerModel.findByIdAndUpdate({_id:id}, updateCustomerDto, {new : true}).select("-__v").exec();;
   }
 
   remove(id: string) {
     return this.CustomerModel.findByIdAndDelete({_id:id}, {new: true}).select("-__v").exec();;
   }
  async findbyloczation(localization: string ){
    console.log(localization);
    
    return this.CustomerModel.findOne({local: localization}).select("-__v").exec();
   }}
