import { Controller, Get, Post, Body, Patch, Param, Delete,Query, Res } from '@nestjs/common';
import { response } from 'express';
import mongoose from 'mongoose';

import { CustomerService } from './customer.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
 async  create(@Res() response ,@Body() createCustomerDto: CreateCustomerDto) {

    const item= await  this.customerService.create(createCustomerDto);
    try{
      return response.status(200).json({
        status:"200",
        message:"Customer created successfully",
        data:{item }     });
    }catch(err){
      return response.status(400).json({
        status:"400",
        message: err.message,
        data:null   });
    }
  }

  @Get()
  findAll() {
    return this.customerService.findAll();
  }
  @Get('/getbyname/:id')
 async findOne(@Param('id') id: string) {
  mongoose.Types.ObjectId.isValid('your id here');
    return this.customerService.findOne(id);
  }

  @Get('localization')
 async  findlocalization(@Query('localization') localization: string) {
    console.log("localization");
    return this.customerService.findbyloczation(localization);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCustomerDto: UpdateCustomerDto) {
    return this.customerService.update(id, updateCustomerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.customerService.remove(id);
  }
}
