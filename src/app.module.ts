import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { CategoryModule } from './category/category.module';
import { SubcategoryModule } from './subcategory/subcategory.module';
import { ProviderModule } from './provider/provider.module';
import { ProductModule } from './product/product.module';
import { CustomerModule } from './customer/customer.module';
import { OrderModule } from './order/order.module';
import { GalleryModule } from './gallery/gallery.module';
import { DelliveryModule } from './dellivery/dellivery.module';
import { MongooseModule } from '@nestjs/mongoose';

import { ConfigModule } from '@nestjs/config';
import { AutheModule } from './authe/authe.module';
import { AuthModule } from './auth/auth.module';


@Module({
  imports: 
  [ConfigModule.forRoot(),UsersModule, CategoryModule, SubcategoryModule, ProviderModule, ProductModule, CustomerModule, OrderModule, GalleryModule, DelliveryModule, MongooseModule.forRoot('mongodb://127.0.0.1:27017/nest'), AutheModule],
 
  controllers: [],
  providers: [],
})
export class AppModule {}
