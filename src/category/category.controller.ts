import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Res } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { response, Response } from 'express';
import { log } from 'console';

@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post()
  async create(@Body() createCategoryDto: CreateCategoryDto, @Res() response: Response) {
    const data= await  this.categoryService.create(createCategoryDto);
    try {
  response.status(201).json({
    status:'201',
    message:"category created successfully",
    data: {
       //id: data.id,
        name: data.name,
        description: data.description,
        subcategories: data.subcategories
    }// populate 
  });
  
 }catch(error){
  response.status(500).json({
    status:'500',
    message:"failed : "+ error.message,
    data: null
  });
 }
  }
  @Get('/byid/:id')
  async  findById(@Param('id') id: string, @Res() response: Response){  
   
     const data= await  this.categoryService.findOne(id);
    try {
      response.status(200).json({
        status:'200',
        message:"category fetched successfully",
        data: data
      })
    } catch (error) {
      response.status(500).json({
        status:'500',
        message: 'failed'+error.message,
        data: null
      })
    }
   }
  
  @Get()
 async  findAll(@Res() response: Response ) {
 const data= await  this.categoryService.findAll();
 try {
  response.status(200).json({
    status:'200',
    message:"category fetched successfully",
    data: data
  });
 }catch (error){
  response.status(500).json({
    status:'500',
    message:"failed : "+ error.message,
    data: null
  });
 }
  }
/*

*/

  @Patch(':id')
  async update(@Res() response: Response , @Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto) {
 const data= await this.categoryService.update(id, updateCategoryDto);
 try {
  response.status(200).json({
    status:'200',
    message:"category updated successfully",
    data: data
  }
    )
 } catch (error) {
  response.status(500).json({
    status:'500',
    message:"failed : "+ error.message,
    data: null
  })
 }
  }

  @Delete(':id')
 async  remove(@Param('id') id: string) {
  const  data = await this.categoryService.remove(id);
  console.log(id);
  
 try {
  response.status(200).json({
    status:'200',
    message:"category deleted successfully",
    data: data
  })
 } catch (error) {
  response.status(error.status).json({
    status:'500',
    message:"failed : "+ error.message,
    data: null
  })
 }
  }
  @Get('name')
  async  findOneByName(@Query('name') name: string, @Res() response: Response){  
   console.log(name);
   
     const data= await  this.categoryService.findOneByName(name);
    try {
      response.status(200).json({
        status:'200',
        message:"category fetched successfully",
        data: data
      })
    } catch (error) {
      response.status(500).json({
        status:'500',
        message: 'failed'+error.message,
        data: null
      })
    }
   


   /*
   @Get(':name')
   async  findOneByName(@Query('name') name: string) :Promise<Category> {  
    
       console.log("name of ", name)
     
       return await this.categoryService.findOneByName(name) ;
    }*/
}}
