import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument,} from "mongoose";
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
export type CategoryDocument=HydratedDocument<Category>;
@Schema()
export class Category {
    @Prop({required: true})
    name:string;
    @Prop()
    description:string;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'subcategories' }], ) 
    subcategories:Types.ObjectId;

}
export const CategorySchema=SchemaFactory.createForClass(Category);