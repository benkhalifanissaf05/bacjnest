import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import {  CategoryDocument } from './entities/category.entity';
import { Category} from './category.interface';
@Injectable()
export class CategoryService {
  constructor(@InjectModel ('categories') private CategoryModel: Model<CategoryDocument>){}
  async  create(createCategoryDto: CreateCategoryDto) :Promise <Category>{
     const creatCategory= new this.CategoryModel(createCategoryDto);
     return await creatCategory.save().then( creatCategory =>creatCategory.populate({path: "subcategories", select: ('-__v -_id -category')}));
    }
     //then(t => t.populate('my-path').execPopulate()
 
  async  findAll() : Promise<Category[]>{
     return this.CategoryModel.find().select("-__v").populate({path: "subcategories", select: ('-__v -_id -category')}).exec();
   }
 
   async findOne(id: string): Promise<Category> {
     return this.CategoryModel.findById(id).select("-__v").populate({path: "subcategories", select: ('-__v -_id -category')}).exec();
   }
 
   async update(id: string, updateCategoryDto: UpdateCategoryDto) {
    console.log("id=  ",id);
    console.log(updateCategoryDto);
    
     return this.CategoryModel.findByIdAndUpdate({_id:id}, updateCategoryDto, {new : true}).select("-__v").populate({path: "subcategories", select: ('-__v -_id -category')}).exec();
   }
 
   async remove(id: string) {
     return this.CategoryModel.findByIdAndDelete({_id:id}, {new: true}).select("-__v").populate({path: "subcategories", select: ('-__v -_id -category')}).exec();
   }
   async findOneByName(name: string) :Promise<any>{
    console.log("name ", name)
     return this.CategoryModel.findOne({name: name}).select("-__v").populate({path: "subcategories", select: ('-__v -_id -category')}).exec();

}}
