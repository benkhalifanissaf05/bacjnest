import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateDelliveryDto } from './dto/create-dellivery.dto';
import { UpdateDelliveryDto } from './dto/update-dellivery.dto';
import { Dellivery, DelliveryDocument } from './entities/dellivery.entity';

@Injectable()
export class DelliveryService {
  constructor(@InjectModel ('delliveries') private DelliveryModel: Model<DelliveryDocument>){}
  async  create(createDelliveryDto: CreateDelliveryDto): Promise<Dellivery> {
     const creatDellivery= new this.DelliveryModel(createDelliveryDto);
     return creatDellivery.save();
   }
 
  async  findAll() : Promise<Dellivery[]>{
     return this.DelliveryModel.find().select("-__v").exec();
   }
 
   async findOne(id: string) {
     return this.DelliveryModel.findById(id).select("-__v").exec();
   }
 
   async update(id: string, updateDelliveryDto: UpdateDelliveryDto) {
     return this.DelliveryModel.findByIdAndUpdate({_id:id}, updateDelliveryDto, {new : true}).select("-__v").exec();;
   } 
 
 async remove(id: string) {
     return this.DelliveryModel.findByIdAndDelete({_id:id}, {new: true}).select("-__v").exec(); ;
   }
  
}
