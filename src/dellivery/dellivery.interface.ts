import { Types } from "mongoose";

export interface Dellivery {
    company: string;
    orders: Types.ObjectId;
}
