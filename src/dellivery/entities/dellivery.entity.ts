
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, Types } from "mongoose";


export type DelliveryDocument =HydratedDocument<Dellivery>;
@Schema()
export class Dellivery {
    @Prop({required: true})
    company: string;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'orders' }])
    orders: Types.ObjectId;
}
export const DelliverySchema= SchemaFactory.createForClass(Dellivery);