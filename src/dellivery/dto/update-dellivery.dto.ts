import { PartialType } from '@nestjs/mapped-types';
import { CreateDelliveryDto } from './create-dellivery.dto';

export class UpdateDelliveryDto extends PartialType(CreateDelliveryDto) {}
