import { Module } from '@nestjs/common';
import { DelliveryService } from './dellivery.service';
import { DelliveryController } from './dellivery.controller';
import { DelliverySchema } from './entities/dellivery.entity';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports:[MongooseModule.forFeature([{name: 'delliveries', schema: DelliverySchema}])],
  controllers: [DelliveryController],
  providers: [DelliveryService]
})
export class DelliveryModule {}
