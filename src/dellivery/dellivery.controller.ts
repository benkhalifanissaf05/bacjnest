import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { DelliveryService } from './dellivery.service';
import { CreateDelliveryDto } from './dto/create-dellivery.dto';
import { UpdateDelliveryDto } from './dto/update-dellivery.dto';

@Controller('dellivery')
export class DelliveryController {
  constructor(private readonly delliveryService: DelliveryService) {}

  @Post()
  async create(@Body() createDelliveryDto: CreateDelliveryDto) {
   const data =await this.delliveryService.create(createDelliveryDto);
  try {
    return

  } catch (error) {
    
  }

  }

  @Get()
  findAll() {
    return this.delliveryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.delliveryService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDelliveryDto: UpdateDelliveryDto) {
    return this.delliveryService.update(id, updateDelliveryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.delliveryService.remove(id);
  }
}
