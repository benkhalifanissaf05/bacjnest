
import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class _JwtAuthGuard extends AuthGuard('jwt') {}
