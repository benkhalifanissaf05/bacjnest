import { PartialType } from '@nestjs/mapped-types';
import { CreateAutheDto } from './create-authe.dto';

export class UpdateAutheDto extends PartialType(CreateAutheDto) {}
