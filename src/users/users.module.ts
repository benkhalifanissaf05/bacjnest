import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import {  MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './entities/user.entity';
import { Provider, ProviderSchema } from 'src/provider/entities/provider.entity';

@Module({
  imports: [ MongooseModule.forFeature([{name : 'users', schema: UserSchema,  discriminators: [
    { name: Provider.name , schema: ProviderSchema },
  ],}],) ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],

})
export class UsersModule {}