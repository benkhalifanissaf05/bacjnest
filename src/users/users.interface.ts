export interface UsersI {
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    email: string;
    mobile: string;
    token: string
}
