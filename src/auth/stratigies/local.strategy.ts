import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { log } from "console";
import { Strategy } from "passport-local";
import { AuthService } from "../auth.service";

@Injectable()
export class  LocalStrategy extends PassportStrategy(Strategy){
    constructor(private readonly authService: AuthService) {
        super();
    }
    async validate(username: string , password:string ): Promise<any>{
        const user= await this.authService.validateUser(username, password)
        console.log('validate fi star',username);
        
        if(!user){
            throw new Error("Invalid Credentials");
            //throw new UnauthorizedException();
        }
        return user;
    }
}