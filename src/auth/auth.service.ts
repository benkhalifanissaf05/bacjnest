import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as argon2 from 'argon2';
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}  async validateUser(username: string, pass: string): Promise<any> {
    console.log('validate user fi autthservice  ',);
    const user = await this.usersService.findOne(username);
    console.log('userr fi validate user',user);
    
    
      const { password, ...result } = user;const passwordMatches = await argon2.verify(user.password,pass);    if (user && passwordMatches) {
      return result;
    }
    return null;
  }  async login(user: any) { 
   
      
     const payload = { username: user.username, sub: user.userId };
    console.log(payload.username);
    
     console.log('login fi service',);
     console.log(payload);
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}