import { Controller, Get, Post, Request, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt.auth.guards';
import { LocalAuthGuard } from './guards/local.auth.guarrs';
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req,@Res() response) {
   
    console.log('username = ',req.user.username);
    
    const data= await this.authService.login(req.user);
    console.log('login fi controller',);
        
    try{
      response.status(200).json({
        message: 'Login Successful',
        data: {data}

      });
    }
    catch(err){
      response.status(401).json({
        message: err.message,
        data: {err}
      });
    }
  }
 
  @UseGuards(JwtAuthGuard)
  @ Get('profile')
  getProfile(@Request() req) {
console.log("profile");

    return req.user;
  }
}
