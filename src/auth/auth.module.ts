import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule} from '@nestjs/jwt';
import { LocalStrategy } from './stratigies/local.strategy';
import { JwtStrategy } from './stratigies/jwt.strategy';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [  ConfigModule.forRoot(),
    UsersModule,PassportModule,
   JwtModule.register({ 
    secret: process.env.JWT_SECRET,
    signOptions: { expiresIn: '60s' },
   // verifyOptions: { algorithms: ['HS256'] }
  }) ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
