import { Module } from '@nestjs/common';
import { GalleryService } from './gallery.service';
import { GalleryController } from './gallery.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { GallerySchema } from './entities/gallery.entity';

@Module({
  imports:[MongooseModule.forFeature([{name: 'galleries', schema: GallerySchema}])],
  controllers: [GalleryController],
  providers: [GalleryService]
})
export class GalleryModule {}
