
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { type } from "os";

export type GalleryDocument =HydratedDocument<Gallery>;
@Schema()
export class Gallery {

    @Prop()
    url_photo: string;
}
export const GallerySchema= SchemaFactory.createForClass(Gallery);