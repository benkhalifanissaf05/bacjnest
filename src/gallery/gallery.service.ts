import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateGalleryDto } from './dto/create-gallery.dto';
import { UpdateGalleryDto } from './dto/update-gallery.dto';
import { Gallery, GalleryDocument } from './entities/gallery.entity';

@Injectable()
export class GalleryService {
  constructor(@InjectModel ('galleries') private GalleryModel: Model<GalleryDocument>){}
  async  create(createGalleryDto: CreateGalleryDto): Promise<Gallery> {
     const creatGallery= new this.GalleryModel(createGalleryDto);
     return creatGallery.save();
   }
 
  async  findAll() : Promise<Gallery[]>{
     return this.GalleryModel.find().exec();
   }
 
   async findOne(id: string) {
     return this.GalleryModel.findById(id).exec();
   }
 
   update(id: string, updateGalleryDto: UpdateGalleryDto) {
     return this.GalleryModel.findByIdAndUpdate({_id:id}, updateGalleryDto, {new : true});
   }
 
   remove(id: string) {
     return this.GalleryModel.findByIdAndDelete({_id:id}, {new: true});
   }
}
