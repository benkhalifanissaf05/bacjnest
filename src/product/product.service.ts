import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product, ProductDocument } from './entities/product.entity';

@Injectable()
export class ProductService {
  constructor(@InjectModel ('products') private ProductModel: Model<ProductDocument>){}
  async  create(createProductDto: CreateProductDto): Promise<Product> {
     const creatProduct= new this.ProductModel(createProductDto);
     return creatProduct.save();
   }
 
  async  findAll() : Promise<Product[]>{
     return this.ProductModel.find().exec();
   }
 
   async findOne(id: string) {
     return this.ProductModel.findById(id).exec();
   }
 
   update(id: string, updateProductDto: UpdateProductDto) {
     return this.ProductModel.findByIdAndUpdate({_id:id}, updateProductDto, {new : true});
   }
 
   remove(id: string) {
     return this.ProductModel.findByIdAndDelete({_id:id}, {new: true});
   }
}
