import { ObjectId, Types } from "mongoose";

export interface Product {
    price: string;
    ref: string;
    qte: string;
    description: string;
    subcategory: ObjectId;
    provider: Types.ObjectId;
    orders: Types.ObjectId;
    galleries: Types.ObjectId;}
