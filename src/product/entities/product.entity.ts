

import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, Types } from "mongoose";
import { type } from "os";
import { Subcategory } from "src/subcategory/entities/subcategory.entity";

export type ProductDocument =HydratedDocument<Product>;
@Schema()
export class Product {

    @Prop()
    price: string;
    @Prop()
    name: string;
    @Prop()
    description: string;
    @Prop()
    ref: string;
    @Prop()
    qte: string;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'subcategories' })

    subcategory: Object;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'providers' })
    provider: Types.ObjectId;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'orders' }])
    orders: Types.ObjectId;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'galleries' }])
    galleries: Types.ObjectId;

 
}
export const ProductSchema= SchemaFactory.createForClass(Product);