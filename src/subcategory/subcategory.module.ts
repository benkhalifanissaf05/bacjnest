import { Module } from '@nestjs/common';
import { SubcategoryService } from './subcategory.service';
import { SubcategoryController } from './subcategory.controller';
import { MongooseModule, Schema } from '@nestjs/mongoose';
import { SubcategorySchema } from './entities/subcategory.entity';

@Module({
  imports:[ MongooseModule.forFeature([{name:'subcategories', schema: SubcategorySchema}])],
 
  controllers: [SubcategoryController],
  providers: [SubcategoryService]
})
export class SubcategoryModule {}
