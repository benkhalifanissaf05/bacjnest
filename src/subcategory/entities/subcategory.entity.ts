
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, Types } from "mongoose";

export type SubCategoryDocument=HydratedDocument<Subcategory>;
@Schema()
export class Subcategory {
    @Prop()
    name:string;
    @Prop()
    description:string;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'categories' })
    category: Types.ObjectId
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }])
    product: Types.ObjectId
}
export const SubcategorySchema=SchemaFactory.createForClass(Subcategory);