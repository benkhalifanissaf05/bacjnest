export class CreateSubcategoryDto {
    name: string;
    description: string;
}
