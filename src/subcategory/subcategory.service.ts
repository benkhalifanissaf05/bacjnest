import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateSubcategoryDto } from './dto/create-subcategory.dto';
import { UpdateSubcategoryDto } from './dto/update-subcategory.dto';
import {  SubCategoryDocument } from './entities/subcategory.entity';
import { Subcategory } from './subcategory.interface';


@Injectable()
export class SubcategoryService {
  constructor(@InjectModel ('subcategories') private subCategoryModel: Model<SubCategoryDocument>){}
 async  create(createSubcategoryDto: CreateSubcategoryDto): Promise<Subcategory> {
    const creatsubcategory= new this.subCategoryModel(createSubcategoryDto);
    return creatsubcategory.save();
  }

 async  findAll() : Promise<Subcategory[]>{
    return this.subCategoryModel.find().select('-_v').populate('name').exec();
  }

  async findOne(id: string) {
    return this.subCategoryModel.findById(id).exec();
  }

  update(id: string, updateSubcategoryDto: UpdateSubcategoryDto) {
    return this.subCategoryModel.findByIdAndUpdate({_id:id}, updateSubcategoryDto, {new : true});
  }

  remove(id: string) {
    return this.subCategoryModel.findByIdAndDelete({_id:id}, {new: true});
  }
}
