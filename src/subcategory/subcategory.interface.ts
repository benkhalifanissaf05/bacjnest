import { Types } from "mongoose";

export interface Subcategory {
    name: string;
    description: string;
    category: Types.ObjectId;
    product: Types.ObjectId
}
